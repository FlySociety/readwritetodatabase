﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace dbReadWrite1
{
    public partial class Form1 : Form
    {
        private SqlConnection conn = null;
        private SqlCommand cmd = null;
        private string origUsername = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            getData();
            dg1.Click += dg1_Click;
            
            txtName.KeyPress += txtName_KeyPress;
            txtName.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            
            txtPWord.KeyPress += txtPWord_KeyPress;
            txtPWord.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            
            formatGrid();
        }

        void txtPWord_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            //If the user presses anything except from backspace
            if (c != 8)
            {
                if ((c < 64 || c > 91) && (c < 96 || c > 123) && (c < 47 || c > 58))
                {
                    e.Handled = true;
                }
            }
        }

        void dg1_Click(object sender, EventArgs e)
        {
            dg1.CurrentRow.Selected = true;
            origUsername = dg1.CurrentRow.Cells[0].Value.ToString();
            txtName.Text = origUsername;
            txtPWord.Text = dg1.CurrentRow.Cells[1].Value.ToString();
            setControlState("u/d");
        }

        private void getData ()
        {
            string connStr = "Data Source=(LocalDB)\\v11.0;AttachDbFilename=C:\\Users\\pat\\Google Drive\\C#\\ReadWriteToDatabase\\dbReadWrite1\\Users.mdf;Integrated Security=True";

            try
            {
                conn = new SqlConnection(connStr);
                conn.Open();
                cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "SELECT * FROM [tUsers]";
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    //Bind and display
                    bindingSource1.DataSource = dr;
                    dg1.DataSource = bindingSource1;
                }
                else
                {
                    //Required to update display when LAST row is DELETED
                    //We should display the column headers
                    //Clears the rows collection but not the heading
                    dg1.Rows.Clear(); 

                }
                dr.Close();
                conn.Close();
                dg1.ClearSelection();
            }
            catch (SqlException ex)
            {
                if (conn != null)
                {
                    conn.Close();
                }
                MessageBox.Show(ex.Message, "Error Reading Data");
            }
        }

        private void cmdInsert_Click(object sender, EventArgs e)
        {
            if (dataGood())
            {
                if (isValidPrimaryKey("i"))
                {
                    string sql = "INSERT INTO [tUsers] ([username], [password]) VALUES ('" + txtName.Text + "', '" + txtPWord.Text + "')";

                    try
                    {
                        conn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        getData();
                        clearText();
                    }
                    catch (SqlException ex)
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                        MessageBox.Show(ex.Message, "Error Inserting Record");
                    }
                }
            }
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (dataGood())
            {
                if (isValidPrimaryKey("u"))
                {
                    string sql = " UPDATE [tUsers] SET [username] = '" + txtName.Text + "', [password] = '" + txtPWord.Text + "' WHERE [username] = '" + origUsername + "'";

                    try
                    {
                        conn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        getData();
                        setControlState("i");
                    }
                    catch (SqlException ex)
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                        MessageBox.Show(ex.Message, "Error Updating Record");
                    }
                }
            }
        }

        private void cmdDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this record?", "Confirm Record Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
            {
                string sql = "DELETE FROM [tUsers] WHERE [username] = '" + origUsername + "'";
                try
                {
                    conn.Open();
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    getData();
                }
                catch (SqlException ex)
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                    MessageBox.Show(ex.Message, "Error Deleting Record");
                }
            }
            setControlState("i");
        }

        private void clearText()
        {
            txtName.Text = "";
            txtPWord.Text = "";
            txtName.Focus();
            dg1.ClearSelection();
        }

        private bool dataGood()
        {
            //Alphanumeric only for username and password
            if (txtName.Text.Length < 1)
            {
                MessageBox.Show("Username required!", "Missing Username", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtName.Focus();
                return false;
            }

            if (txtName.Text.Length < 1)
            {
                MessageBox.Show("Password required!", "Missing Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPWord.Focus();
                return false;
            }
            return true;
        }

        private bool isValidPrimaryKey(string state)
        {
            //TODO
            if (state.Equals("i"))
            {
                for (int i = 0; i < dg1.Rows.Count; i++)
                {
                    if (txtName.Text.ToLower().Equals(dg1.Rows[i].Cells[0].Value.ToString().ToLower()))
                    {
                        MessageBox.Show("This username exists, enter new username!", "Primary Key Violation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtName.Focus();
                        return false;
                    }
                }
            }
            else if (state.Equals("u"))
            {
                for (int i = 0; i < dg1.Rows.Count; i++)
                {
                    if (i != dg1.CurrentRow.Index)
                    {
                        if (txtName.Text.ToLower().Equals(dg1.Rows[i].Cells[0].Value.ToString().ToLower()))
                        {
                            MessageBox.Show("This username does not exist, enter new username!", "Invalid Username", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtName.Focus();
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void setControlState(string state)
        {
            //TODO
            if (state.Equals("i"))
            {
                cmdInsert.Enabled = true;
                cmdUpdate.Enabled = false;
                cmdDelete.Enabled = false;
                clearText();
            }
            else if (state.Equals("u/d"))
            {
                cmdInsert.Enabled = false;
                cmdUpdate.Enabled = true;
                cmdDelete.Enabled = true;
            }
        }

        private void formatGrid()
        {
            dg1.ColumnHeadersDefaultCellStyle.BackColor = Color.Aqua;
            dg1.RowHeadersDefaultCellStyle.BackColor = Color.Aqua;

            dg1.RowsDefaultCellStyle.BackColor = Color.LawnGreen;
            dg1.AlternatingRowsDefaultCellStyle.BackColor = Color.Yellow;

            dg1.RowsDefaultCellStyle.SelectionBackColor = Color.Orange;
            dg1.RowHeadersDefaultCellStyle.SelectionBackColor = Color.Orange;
        }
    }
}
